# Stage 1 -- Set-up

# pull the official base image  
FROM node:13.12.0-alpine AS react-build
 
# set your working directory  
WORKDIR /app
 
# add `/app/node_modules/.bin` to $PATH  
# ENV PATH /app/node_modules/.bin:$PATH  
 
# install application dependencies  
COPY package.json ./
RUN npm install --silent
 
# add app  
COPY . ./  

# Building app
RUN npm run runBuild
 
# will start app  
#CMD ["npm", "start"]


# Stage 2 -- Production

# Stage 2 - the production environment
FROM nginx:alpine
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=react-build /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

