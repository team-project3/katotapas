import React, { useEffect, useRef, useState } from "react";
import lottie from "lottie-web";
import { Link } from "react-router-dom";
import "../App.css";
import UnopDropdown from "unop-react-dropdown";

const ShoppingCart = (props) => { 
  return (
      <div className = "flex justify-start">
         <div className="border-l border-b border border-black w-full p-4 bg-white">
          <h1 className="font-sans font-bold">
              Your order
          </h1>
          <div className="font-sans font-bold text-yellow-600">
              Katotapas
          </div>
          <div className = "grid grid-cols-3 gap-2 py-6">
                <div className="grid row-span-2 justify-start">
                    1x
                </div>
                <div className = "flex justify-center">
                    Seven-up 
                </div>
                <div className="grid row-span-2 justify-end">
                    5 PLN
                </div>
                <div className="flex justify-center">
                    <button className="font-bold text-yellow-500">
                        Remove
                    </button>
                </div>

                <div className="grid row-span-2 justify-start">
                    1x
                </div>
                <div className = "flex justify-center">
                    Seven-up 
                </div>
                <div className="grid row-span-2 justify-end">
                    5 PLN
                </div>
                <div className="flex justify-center">
                    <button className="font-bold text-yellow-500">
                        Remove
                    </button>
                </div>

                <div className="grid row-span-2 justify-start">
                    1x
                </div>
                <div className = "flex justify-center">
                    Seven-up 
                </div>
                <div className="grid row-span-2 justify-end">
                    5 PLN
                </div>
                <div className="flex justify-center"> 
                    <button className="font-bold text-yellow-500">
                        Remove
                    </button>
                </div>

                <div className="grid row-span-2 justify-start">
                    1x
                </div>
                <div className = "flex justify-center">
                    Seven-up 
                </div>
                <div className="grid row-span-2 justify-end">
                    5 PLN
                </div>
                <div className="flex justify-center">
                    <button className="font-bold text-yellow-500">
                        Remove
                    </button>
                </div>
          </div>
          <div className="flex justify-between bg-yellow-500 rounded">
            <div className="flex justify-start text-white py-2 px-2">
                <a href="/payment">
                    {props.msg}
                </a>
            </div>
            <div className="flex justify-end text-white py-2 px-2">
                20 PLN
            </div>
          </div>
        </div>  
      </div> 
      
  )
}

export default ShoppingCart