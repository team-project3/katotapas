import React from "react";
import "../App.css";
import ProductPopup from '../components/ProductPopup';
import { CustomDialog } from 'react-st-modal';

const Product = (props) => {
  return (
    <button onClick={async () => {
      const result = await CustomDialog(
        <ProductPopup {...props}/>
      );
    }}>
      <div className=" border border-gray-500 flex">
        <div className="grid grid-cols-3">
          <div className="grid col-span-2 py-2">
            <h1 className="flex justify-center text-xl font-semibold">
              {props.name}
            </h1>
          </div>
          <div className="grid row-span-3 w-36 h-48 relative ">
            <img src={props.image} alt="Error load" className="absolute inset-0 w-full h-full object-cover pxl-4 py-0.5 flex justify-end" />
          </div>
          <div className="grid col-span-2 w-full flex-none text-sm font-medium text-gray-500 justify-center py-2">
            {props.description}
          </div>
          <div className="grid col-span-2 text-xl font-semibold text-gray-500 justify-center py-2">
            {props.price} PLN
          </div>
        </div>
      </div>
      <div class="p-5">
      </div>
    </button>

  )
}

export default Product
