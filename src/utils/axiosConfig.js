import axios from 'axios'

const instance = axios.create({
    baseURL: "https://katobackend.azurewebsites.net"
})

export default instance