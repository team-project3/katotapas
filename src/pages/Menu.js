import React from "react";
import "../App.css";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import Product from "../components/Product";
import { Link } from 'react-scroll';

//Drinks images
import sevenupImage from '../assets/images/7up.jpg'
import waterImage from '../assets/images/Water.jpg'
import cocaColaImage from '../assets/images/cocacola.png'
import fantaImage from '../assets/images/fanta.png'
import nesteaImage from '../assets/images/nestea.jpg'

//Tapas images
import potatoesAliOliImage from '../assets/images/patatasalioli.jpg'
import ensaladillaDeGambasImage from '../assets/images/ensaladillagambas.jpg'
import chocosFritosImage from '../assets/images/chocosfritos.jpg'
import atunAlAjilloImage from '../assets/images/atunalajillo.jpg'
import deliciaIbericaImage from '../assets/images/surtidoiberico.jpg'
import croquetasCocidoImage from '../assets/images/croquetasdecocido.jpg'
import caracoleImage from '../assets/images/caracoles.jpg'
import bacalaoDouroImage from '../assets/images/bacalaoaldouro.jpg'
import salmorejoImage from '../assets/images/salmorejo.jpg'

//Individual plates images
import HamburguerImage from '../assets/images/hamburguer.jpg'
import hawaiianPizzaImage from '../assets/images/hawaiianpizza.jpg'
import bbqRibsImage from '../assets/images/ribs.jpg'
import cachopoImage from '../assets/images/cachopo.jpg'
import montaditoCalamaresImage from '../assets/images/montaditocalamares.jpg'

//Desserts images
import flanImage from '../assets/images/flan.jpg'
import cheeseCakeImage from '../assets/images/cheesecake.jpg'
import puddingImage from '../assets/images/arrozconleche.jpg'
import threeChocolateCakeImage from '../assets/images/tartade3chocolates.jpg'

export default function Landing() {

  return (
    <div>
      <Navbar />
      <div className="flex justify-center sticky top-0 z-40 bg-white">
        <div className="flex justify-center font-serif text-2xl border-b border-gray-200 w-1/4 mb-4">
          <Link to="tapas" smooth activeClass="active font-bold" spy className="m-3">
            Tapas
          </Link>
          <Link to="meals" smooth activeClass="active font-bold" spy className="m-3">
            Meals
          </Link>
          <Link to="desserts" smooth activeClass="active font-bold" spy className="m-3">
            Desserts
          </Link>
          <Link to="drinks" smooth activeClass="active font-bold" spy className="m-3">
            Drinks
          </Link>
        </div>
      </div>
      <div className="flex justify-center">
        <div className="mx-96 grid gap-8 grid-cols-2 auto-cols-max">
          <h2 className="font-bold flex justify-right title-font sm:text-4xl text-3xl mb-4 text-gray-900 font-serif" id="tapas">
            Tapas
          </h2>
          <div></div>
          <div>
            <Product name="Patatas con alioli" price={20} description="Roasted potato with homemade aioli sauce y de mas cosas" image={potatoesAliOliImage}></Product>
          </div>
          <div>
            <Product name="Ensaladilla de gambas" price={20} description="Mix of potatoes, eggs, mayo, tuna and prawns" image={ensaladillaDeGambasImage}></Product>
          </div>
          <div>
            <Product name="Chocos fritos" price={25} description="Fried cuttlefish from the atlantic ocean. Lemon included" image={chocosFritosImage}></Product>
          </div>
          <div>
            <Product name="Atún al ajillo" price={25} description="Grilled tuna steaks with oil, garlic and parsley" image={atunAlAjilloImage}></Product>
          </div>
          <div>
            <Product name="Delicia ibérica" price={30} description="Selection of Serrano ham, Iberian pork loin and cured Manchego cheese" image={deliciaIbericaImage}></Product>
          </div>
          <div>
            <Product name="Croquetas de cocido" price={15} description="Homemade Madrid stew meat croquettes with béchamel sauce" image={croquetasCocidoImage}></Product>
          </div>
          <div>
            <Product name="Snails" price={18} description="Snails simmered in concentrated meat broth" image={caracoleImage}></Product>
          </div>
          <div>
            <Product name="Bacalao al douro" price={29} description="Scrambled cod with potatoes, onion and eggs" image={bacalaoDouroImage}></Product>
          </div>
          <div>
            <Product name="Salmorejo de tomate" price={13} description="Cold tomato cream with ham and fried breadcrumbs" image={salmorejoImage}></Product>
          </div>

          <div></div>

          <h2 className="font-bold flex justify-right title-font sm:text-4xl text-3xl mb-4 text-gray-900 font-serif" id="meals">
            Meals
          </h2>
          <div></div>
          <div>
            <Product name="American Hamburguer" price={29} description="Beef burguer on bun with onion, tomato, sla, cheese, mayonaise and ketchup. Large fries included" image={HamburguerImage}></Product>
          </div>
          <div>
            <Product name="Hawaiian Pizza" price={35} description="Tomato base with melted cheese, york and pineapple in the most Hawaiian style. Size: 26cm" image={hawaiianPizzaImage}></Product>
          </div>
          <div>
            <Product name="BBQ Ribs" price={50} description="Smoked Pork Ribs, bathed in Jack Daniel's BBQ Sauce" image={bbqRibsImage}></Product>
          </div>
          <div>
            <Product name="Cachopo" price={27} description="Breaded beef fillet stuffed with Iberian ham and cheese. Large fries included" image={cachopoImage}></Product>
          </div>
          <div>
            <Product name="Montadito of squids" price={18} description="Sandwich of homemade squids with aioli sauce" image={montaditoCalamaresImage}></Product>
          </div>
          <div></div>

          <h2 className="font-bold flex justify-right title-font sm:text-4xl text-3xl mb-4 text-gray-900 font-serif" id="desserts">
            Desserts
          </h2>
          <div></div>
          <div>
            <Product name="Flan" price={20} description=" Portuguese egg flan with caramel, pine nuts and sugar" image={flanImage}></Product>
          </div>
          <div>
            <Product name="Cheesecake" price={26} description="Creamy cake with a biscuit base, Philadelphia cheese and strawberry jam" image={cheeseCakeImage}></Product>
          </div>
          <div>
            <Product name="Rice pudding" price={23} description="Rice with condensed milk curdled with a touch of cinnamon" image={puddingImage}></Product>
          </div>
          <div>
            <Product name="Three chocolate cake" price={20} description="Biscuit base cake with dark, milk and white chocolate" image={threeChocolateCakeImage}></Product>
          </div>

          <div>
            <h2 className="font-bold flex justify-right title-font sm:text-4xl text-3xl mb-4 text-gray-900 font-serif" id="drinks">
              Drinks
            </h2>
          </div>
          <div></div>
          <div>
            <Product name="7Up" price={5} description="Sweet fizzy drink" image={sevenupImage}></Product>
          </div>
          <div>
            <Product name="Water" price={3} description="Still water" image={waterImage}></Product>
          </div>
          <div>
            <Product name="CocaCola" price={5} description="Classic cocacola fizzy drink" image={cocaColaImage}></Product>
          </div>
          <div>
            <Product name="Orange Fanta" price={5} description="Fizzy drink orange flavored" image={fantaImage}></Product>
          </div>
          <div>
            <Product name="Nestea" price={5} description="Sweet tea drink" image={nesteaImage}></Product>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
