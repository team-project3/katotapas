import React from "react";
import "../App.css";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import Product from "../components/Product";
import { Link } from 'react-scroll';
import ShoppingCart from "../components/ShoppingCart";

export default function Landing() {

    return (
      <div>
        <Navbar />

        <section className = "my-60">
        <div className = "flex flex-row">
          <div className="flex-1 px-4">
            <h2 className = "uppercase tracking-wide text-black font-bold mb-2">
              Card information
            </h2>

            <label className="uppercase tracking-wide text-black text-xs font-bold mb-2">
              Card number
            </label>
            <input className="w-full bg-gray-200 text-black border border-gray-200 rounded py-3 px-4 mb-3" id="company" type="text" placeholder="XXXX XXXX XXXX XXXX"></input>

            <label className="uppercase tracking-wide text-black text-xs font-bold mb-2">
              Date of Expiry
            </label>
            <div className = "flex flex-row">
              <div className = "pxr-4">
                <label className="uppercase tracking-wide text-black text-xs font-bold mb-2">
                Month
                </label>
                <input className="w-full bg-gray-200 text-black border border-gray-200 rounded  py-3 px-4 mb-3" id="company" type="text" placeholder="MM"></input>
              </div>
              <div className = "px-4">
                <label className="uppercase tracking-wide text-black text-xs font-bold mb-2">
                  Year
                </label>
                <input className="w-full bg-gray-200 text-black border border-gray-200 rounded  py-3 px-4 mb-3" id="company" type="text" placeholder="YYYY"></input>
              </div>
            </div>

            <label className="uppercase tracking-wide text-black text-xs font-bold mb-2">
              Security code
            </label>
            <input className="w-full bg-gray-200 text-black border border-gray-200 rounded py-3 px-4 mb-3" id="company" type="text" placeholder="CVV"></input>

            <label className="uppercase tracking-wide text-black text-xs font-bold mb-2">
              Cardholder
            </label>
            <input className="w-full bg-gray-200 text-black border border-gray-200 rounded py-3 px-4 mb-3" id="company" type="text"></input>
          </div>

          <div className = "flex-1 px-4">
            <h2 className = "uppercase tracking-wide text-black font-bold mb-2">
              Shipping address
            </h2>

            <div className = "flex flex-row">
              <div className = "px-2">
                <label className="uppercase tracking-wide text-black text-xs font-bold mb-2">
                  City
                </label>
                <input className="w-full bg-gray-200 text-black border border-gray-200 rounded py-3 px-4 mb-3" id="company" type="text"></input>
              </div>

              <div className = "px-2">
                <label className="uppercase tracking-wide text-black text-xs font-bold mb-2">
                  Postal code
               </label>
                <input className="w-full bg-gray-200 text-black border border-gray-200 rounded py-3 px-4 mb-3" id="company" type="text" placeholder="XX-XXX"></input>
              </div>
            </div>

            <label className="uppercase tracking-wide text-black text-xs font-bold mb-2">
                  Street and number 
            </label>
            <input className="w-full bg-gray-200 text-black border border-gray-200 rounded py-3 px-4 mb-3" id="company" type="text"></input>

            <label className="uppercase tracking-wide text-black text-xs font-bold mb-2">
              Apartament
            </label>
            <input className="w-full bg-gray-200 text-black border border-gray-200 rounded py-3 px-4 mb-3" id="company" type="text"></input>
          </div>

          <div className="flex-1 px-4">
            <ShoppingCart msg="Pay">
              
            </ShoppingCart>
          </div>

        </div>
       </section>

        <Footer />
      </div>
    );
  }